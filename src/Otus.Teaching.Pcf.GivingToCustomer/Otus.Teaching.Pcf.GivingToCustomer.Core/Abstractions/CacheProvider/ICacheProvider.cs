﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.CacheProvider
{
    public interface ICacheProvider
    {
        Task<T> GetByKeyAsync<T>(string key, CancellationToken ct = default);
        void AddObjectToCache<T>(string key, T obj, TimeSpan lifeTime);
    }
}
