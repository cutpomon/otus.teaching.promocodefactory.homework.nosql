﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.CacheProvider;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly ICacheProvider _cacheProvider;

        public PreferencesController(
            IRepository<Preference> preferencesRepository,
            ICacheProvider cacheProvider)
        {
            _preferencesRepository = preferencesRepository;
            _cacheProvider = cacheProvider;
        }
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            string cacheKey = "preferences";

            var cachedResponse = await _cacheProvider.GetByKeyAsync<List<PreferenceResponse>>(cacheKey);
            if (cachedResponse is not null)
                return cachedResponse;

            var preferences = await _preferencesRepository.GetAllAsync();

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            _cacheProvider.AddObjectToCache(cacheKey, response, new TimeSpan(1, 0, 0));

            return Ok(response);
        }
    }
}