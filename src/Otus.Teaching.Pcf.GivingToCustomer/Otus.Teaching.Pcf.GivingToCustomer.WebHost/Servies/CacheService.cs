﻿using Microsoft.Extensions.Caching.Distributed;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.CacheProvider;
using System.Threading.Tasks;
using System.Threading;
using System;
using System.Text.Json;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Servies
{
    public class CacheService : ICacheProvider
    {
        private readonly IDistributedCache _cache;

        public CacheService(IDistributedCache cache)
        {
            _cache = cache;
        }

        public void AddObjectToCache<T>(string key, T obj, TimeSpan lifeTime)
        {
            _cache.SetString(key, JsonSerializer.Serialize(obj), new DistributedCacheEntryOptions()
            {
                AbsoluteExpirationRelativeToNow = lifeTime
            });
        }

        public async Task<T> GetByKeyAsync<T>(string key, CancellationToken ct = default)
        {
            string cachedResponse = await _cache.GetStringAsync(key, ct);

            return cachedResponse is null
                ? default
                : JsonSerializer.Deserialize<T>(cachedResponse);
        }
    }
}
